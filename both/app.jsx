App = React.createClass({
  mixins: [ReactMeteorData],
  getMeteorData(){
    return{
      social: Social.find({},{sort: {the_time: -1}}).fetch()
    }
  },
  showInsta(){
    return(
      <div key={thing.id} className="social">
          <h2>{thing.caption.text}</h2>
          <img src={thing.images.low_resolution.url}/>

      </div>
    )
  },
  showTweet(thing){
    return(

      <div key={thing.id} className="social">

        <h2>{thing.text}</h2>
        <h3>{thing.user.screen_name}</h3>
      {(thing.entities.media) ? <img src={thing.entities.media[0].media_url}/> : ''}

      </div>
    )
  },
  render(){
    return(
      <div>
        {this.data.social.map(function(thing){

          if(thing.caption){


          return(

            <Instagram key={thing._id} data={thing}/>
          )
        }else{
          return(
            <Tweet key={thing._id} data={thing}/>
          )
        }
        })}
      </div>
    )
  }
})


Instagram = React.createClass({
  render(){
    var gram = this.props.data
    return(
      <div key={gram._id} className="social">
        <h1>Gram</h1>
        <h2>{gram.caption.text}</h2>
        <img src={gram.images.low_resolution.url}/>
      </div>
    )
  }
})

Tweet = React.createClass({
  render(){
    var tweet = this.props.data
    return(
      <div key={tweet.id} className="social">
        <h1>Tweet</h1>
        <h2>{tweet.text}</h2>
        <h3>{tweet.user.screen_name}</h3>
      {(tweet.entities.media) ? <img src={tweet.entities.media[0].media_url}/> : ''}

      </div>

    )
  }
})
